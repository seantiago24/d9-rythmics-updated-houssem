/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.bootstrap_custom = {
    attach: function (context, settings) {

      // var data = drupalSettings.bootstrap_custom;
      // console.log(drupalSettings)


    }
  };



  Drupal.behaviors.select = {
    attach: function (context, settings) {


      // $(document).ready(function(){
      //   $(".bef-slider span").bind('change', function(event) {
      //     // console.log('1212212')
      //     // alert( $("#selected-date-range").attr("value") );
      //   });
      // });

      $(".custom-colors-select option").click(function () {
        var attr = $(this).attr('selected');

        if (typeof attr !== 'undefined' && attr !== false) {
          $(this).removeAttr('selected');
          $(this).addClass('unchecked');
          $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
          // console.log('ooooooook')
        }else{
          $(this).attr('selected','selected')
        }

      });





      $('.sort_dropdown').on('change', function() {
        $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
      });

      $('#edit-price-number').on('change', function() {
        $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
      });

      $('.bef-checkboxes').on('change', function() {
        $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
      });

      // $('.bef-slider.ui-slider.ui-corner-all>span').on('change', function() {
      //   $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
      // });

      $('.custom-colors-select').on('change', function() {
        // $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
      });

      $('.custom-select-material').on('change', function() {
        $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
      });
      $('.form-autocomplete').on('change', function() {
        $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
      });

      // $("select").on("changed.bs.select",
      //   function(e, clickedIndex, newValue, oldValue) {
      //     $('.views-exposed-form').find('.form-filter-submit').click();
      //   });


      // $(window).on('load', function(){
      //   $('.loader').fadeOut(200);
      // })


      // $(function () {
      //   $('[name="sort_bef_combine"]').selectpicker({
      //     noneSelectedText: 'Sort By',
      //     liveSearch: false,
      //     maxOptions: 1,
      //   });
      // });


    }
  };

// Prevent closing from click inside dropdown
  $(document).on('click', '.dropdown-menu', function (e) {
    e.stopPropagation();
  });

  // make it as accordion for smaller screens
  if ($(window).width() < 992) {
    $('.dropdown-menu a').click(function(e){
      if($(this).attr('href') == '#')
        e.preventDefault();
      if($(this).next('.submenu').length){
        $(this).next('.submenu').toggle();
      }
      $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.submenu').hide();
      })
    });
  }


})(jQuery, Drupal);
