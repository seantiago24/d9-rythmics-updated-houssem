(function ($, Drupal, drupalSettings) {
  /**
   * @namespace
   */
  Drupal.behaviors.mymoduleAccessData = {
    attach: function (context, settings) {
      $(document, context).once('my_behavior ').each(function () {

        var data = drupalSettings.bootstrap_custom;

        $(".btn__selected_filters").on("click", function () {
          var id = $(this).attr('id');

          var inputs = $('.bef-checkboxes').find("input[value-id='" + id + "']")
          if (inputs.attr('checked')) {
            inputs.removeAttr('checked')
            // $(this).parents('.views-exposed-form').find('.form-filter-submit').click();
            $('.form-filter-submit').click();
          }
        });
      });

    }
  };
})(jQuery, Drupal, drupalSettings);



